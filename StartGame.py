import pygame as pg
import os
from GameGraphics import GameGraphics
from FileReader import FileReader

os.environ['SDL_VIDEO_CENTERED'] = '1'
pg.init()
w,h = (pg.display.Info().current_w, pg.display.Info().current_h)
screen = pg.display.set_mode((int(w//2),int(h * 0.85)))
clicked = False
btn = -1
def getBotaoPosition(number):
    w,h = (pg.display.Info().current_w, pg.display.Info().current_h)
    botaoPositionX = 25
    botaoHeightBase = h//10
    botaoPositionY = 20 + h//5 + h*0.03 + ((botaoHeightBase + h*0.05) * number)
    return (botaoPositionX, botaoPositionY)

def getChangesOptions(text, value, width):
    painel = pg.font.Font(None, 45)
    s = pg.Surface((width,50),pg.SRCALPHA)
    s.set_alpha(0)
    vm = pg.Surface((100,70))
    vm.fill((0,0,0))
    texto = painel.render(text,0,(2,104,36))
    value = painel.render(str(value),0,(255,0,0))
    vm.blit(value, (vm.get_width()//2 - value.get_width()//2, vm.get_height()//2 - value.get_height()//2))
    s.blit(lt, (50,0))
    s.blit(vm, (100,0))
    s.blit(rt,(200,0))
    s.blit(texto,(305,10))
    return s

musicVolume = 100
soundVolume = 100
totalTime = 60
window = pg.Surface((w,h))
botao = pg.image.load("images/botao.png")
botao = pg.transform.scale(botao, (w//2 - 50,h//10))
backGround = pg.image.load("images/Kids-Fruit-bg.jpg")
backGround = pg.transform.scale(backGround, (int(w//2),int(h * 0.85)))
bts = []
opcoes = []

gg = None
font = pg.font.Font('fonts/Flavors-Regular.ttf', 60)
botao1Txt = font.render("Inicia o Jogo",0,((2,104,36)))
botao2Txt = font.render("Opcões",0,((2,104,36)))
botao3Txt = font.render("Pontuação",0,((2,104,36)))
botao4Txt = font.render("Créditos",0,((2,104,36)))
window.blit(backGround, (0,0))
mainSurface = pg.Surface((w//2,h * 0.85), pg.SRCALPHA)
bts.append(pg.draw.rect(mainSurface,(0,0,255,0),pg.Rect(getBotaoPosition(0),(w//2 - 50,h//10))))
bts.append(pg.draw.rect(mainSurface,(0,0,255,0),pg.Rect(getBotaoPosition(1),(w//2 - 50,h//10))))
bts.append(pg.draw.rect(mainSurface,(0,0,255,0),pg.Rect(getBotaoPosition(2),(w//2 - 50,h//10))))
bts.append(pg.draw.rect(mainSurface,(0,0,255,0),pg.Rect(getBotaoPosition(3),(w//2 - 50,h//10))))
txtVoltar = font.render("Voltar",0,(2,104,36))
dimentions = (txtVoltar.get_width(),txtVoltar.get_height())
voltar = pg.draw.rect(screen,(255,0,0),pg.Rect((250, getBotaoPosition(3)[1] + 20),dimentions))
opcoes.append(pg.draw.rect(screen,(255,0,0,0),pg.Rect((75, 120),(50,50))))
opcoes.append(pg.draw.rect(screen,(255,0,0,0),pg.Rect((225, 120),(50,50))))
opcoes.append(pg.draw.rect(screen,(255,0,0,0),pg.Rect((75, 220),(50,50))))
opcoes.append(pg.draw.rect(screen,(255,0,0,0),pg.Rect((225, 220),(50,50))))
opcoes.append(pg.draw.rect(screen,(255,0,0,0),pg.Rect((75, 320),(50,50))))
opcoes.append(pg.draw.rect(screen,(255,0,0,0),pg.Rect((225, 320),(50,50))))
mainSurface.fill((242,239,206,10))
mainSurface.blit(botao, getBotaoPosition(0))
mainSurface.blit(botao1Txt,(w//4 - botao1Txt.get_width()//2, getBotaoPosition(0)[1]))
mainSurface.blit(botao, getBotaoPosition(1))
mainSurface.blit(botao2Txt,(w//4 - botao2Txt.get_width()//2, getBotaoPosition(1)[1]))
mainSurface.blit(botao, getBotaoPosition(2))
mainSurface.blit(botao3Txt,(w//4 - botao3Txt.get_width()//2, getBotaoPosition(2)[1]))
mainSurface.blit(botao, getBotaoPosition(3))
mainSurface.blit(botao4Txt,(w//4 - botao4Txt.get_width()//2, getBotaoPosition(3)[1]))
window.blit(mainSurface, (0, 0))
rt = pg.image.load("images/seta_direita.png")
rt = pg.transform.scale(rt,(50,50))
lt = pg.image.load("images/seta_esquerda.png")
lt = pg.transform.scale(lt,(50,50))

btVoltar = pg.Surface(dimentions)
pg.draw.rect(btVoltar,(255,0,0),pg.Rect((0,0),dimentions))
pg.draw.rect(btVoltar,(0,0,0),pg.Rect((2, 2),(txtVoltar.get_width()-5,txtVoltar.get_height()-5)),5)
btVoltar.blit(txtVoltar,(0,0))

screen.blit(window, (0,0))
pg.display.update()
run = True
contador = 0
pos = (-1,-1)

def getOpcoesSurface():
    fr = FileReader("abc.txt")
    s =fr.getInfo((botao.get_width(),int(h * 0.7)))
    s.blit(getChangesOptions("Volume das musicas", musicVolume, s.get_width()), (0,100))
    s.blit(getChangesOptions("Som", soundVolume, s.get_width()), (0,200))
    s.blit(getChangesOptions("Tempo", totalTime, s.get_width()), (0,300))
    return s

while run:
    for event in pg.event.get():
        if event.type == pg.KEYDOWN and event.key == pg.K_ESCAPE:
            run = False
        if event.type == pg.QUIT:
            run = False
        if event.type == pg.MOUSEBUTTONUP:
            pos = pg.mouse.get_pos()
            if not clicked:
                for bt in bts:
                    if bt.collidepoint(pos):
                        #clicked = True
                        id = bts.index(bt)
                        if id == 0:
                            try:
                                if gg is None:
                                    gg = GameGraphics(True)
                                else:
                                    gg.resetGame()
                                gg.setMusicVolume(musicVolume/100)
                                gg.setSoundsVolume(soundVolume/100)
                                gg.setTotalTime(totalTime)
                                #screen = gg.window
                                gg.start()
                            except Exception as e:
                                 print(e)
                                 print("O jogo Crashou!!")
                                 run = False
                            #pg.quit()
                            screen = pg.display.set_mode((int(w//2),int(h * 0.85)))
                            screen.blit(window, (0,0))
                            clicked = False
                        if id == 1:
                            s = getOpcoesSurface()
                            screen.blit(s,(25, 20))
                            screen.blit(btVoltar, (250, getBotaoPosition(3)[1] + 20))
                            contador = 0
                            btn = 1
                        if id == 2:
                            fr = FileReader("score.txt", "score_names.txt")
                            s =fr.getInfo((botao.get_width(),int(h * 0.7)))
                            screen.blit(s,(25, 20))
                            screen.blit(btVoltar, (250, getBotaoPosition(3)[1] + 20))
                            contador = 0
                            btn = 2
                        if id == 3:
                            fr = FileReader("nomes.txt")
                            s = fr.getInfo((botao.get_width(),int(h * 0.7)))
                            screen.blit(s,(25, 20))
                            screen.blit(btVoltar, (250, getBotaoPosition(3)[1] + 20))
                            contador = 0
                            btn = 3
    if voltar.collidepoint(pos) and clicked:
        screen.blit(window, (0,0))
        clicked = False
        btn = -1

    if btn > 0 and not clicked:
        contador += 1
        pos = (-1,-1)
        clicked = True

    if clicked and btn == 1:
        for op in opcoes:
            if op.collidepoint(pos):
                acao  = opcoes.index(op)
                if acao == 0:
                    if musicVolume <= 10:
                        musicVolume = 00
                    else:
                        musicVolume = musicVolume - 10
                if acao == 1:
                    if musicVolume >= 90:
                        musicVolume = 100
                    else:
                        musicVolume = musicVolume + 10
                if acao == 2:
                    if soundVolume <= 10:
                        soundVolume = 00
                    else:
                        soundVolume = soundVolume - 10
                if acao == 3:
                    if soundVolume >= 90:
                        soundVolume = 100
                    else:
                        soundVolume = soundVolume + 10
                elif acao == 4:
                    if totalTime == 99:
                        totalTime = 90
                    elif totalTime <= 20:
                        totalTime = 10
                    else:
                        totalTime = totalTime - 10
                elif acao == 5:
                    if totalTime >= 90:
                        totalTime = 99
                    else:
                        totalTime = totalTime + 10
                s = getOpcoesSurface()
                screen.blit(s,(25, 20))
                screen.blit(btVoltar, (250, getBotaoPosition(3)[1] + 20))



        pos = (-1,-1)

    pg.display.update()
    pg.time.delay(40)
pg.quit()




