import pygame
from Game import Game
from Moviment import Moviment as mv

pygame.init()
game = Game()
board = game.board
window = pygame.display.set_mode((500,500))
pygame.display.set_caption("FruiThree")
bg = pygame.image.load("images/bg_2.jpg")
bg = pygame.transform.scale(bg, (500,500))
frutas = []
retangulos = []
window.fill((0,0,0))
window.blit(bg,(0,0))
s = pygame.Surface((440,440), pygame.SRCALPHA)
s.fill((0,0,180,170))
window.blit(s,(20,20))
for i in range(0,board.getBoardHight()):
    for j in range(0,board.getBoardWidth()):
        if j == 0:
            frutas.append([board.getBlock(i,j).getFoodRepresentant()])
        else:
            frutas[i].insert(j,board.getBlock(i,j).getFoodRepresentant())
        fx = 40 + j * 80
        fy = 40 + i * 80
        #retangulos[i][j] = pygame.draw.rect(window,(0,0,0),(fx,fy,80,80))
        window.blit(frutas[i][j],(fx,fy))
#morango = pygame.image.load("images/if_Strawberry_56028.png")
#morango = pygame.transform.scale(morango, (80,80))
run = True
x = 0
y = 0
velocity = 5
left = False
retangulo = None
pos = (0,0)
mov = mv.LEFT
collide = False
while run:
    pygame.time.delay(40)

    sleft,middle,right = (False,False,False)
    mx,my = (0,0)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
        if event.type == pygame.MOUSEBUTTONDOWN :
            #sleft,middle,right =  pygame.mouse.get_pressed()
            cx,cy = pygame.mouse.get_pos()
            ci,cj = (-1,-1)
            if cx < 40 or cx > 460:
                collide = False
            elif cy < 40 or cy > 460:
                collide = False
            else:
                collide = True
                cj = (cx - 40)//80
                ci = (cy - 40)//80
                if mov == mv.LEFT:
                    game.swapLeft(ci,cj)
                if mov == mv.RIGHT:
                    game.swapRight(ci,cj)
                if mov == mv.UP:
                    game.swapUp(ci,cj)
                if mov == mv.DOWN:
                    game.swapDown(ci,cj)
                pos = (0,0)
            #rx,ry = pygame.mouse.get_rel()
            #print("Mouse Bottom DOWN REL: ", rx, ", ", ry)
        if event.type == pygame.MOUSEBUTTONUP :
            #rx,ry = pygame.mouse.get_rel()
            #print("Mouse Bottom UP REL: ", rx, ", ", ry)
            window.blit(bg,(0,0))
            window.blit(s,(20,20))
            for i in range(0,board.getBoardWidth()):
                for j in range(0,board.getBoardHight()):
                    fx = 40 + j * 80
                    fy = 40 + i * 80
                    frutas[i][j] = board.getBlock(i,j).getFoodRepresentant()
                    window.blit(frutas[i][j],(fx,fy))



    keys = pygame.key.get_pressed()

    if keys[pygame.K_LEFT]:
        mov = mv.LEFT
    if keys[pygame.K_RIGHT]:
        mov = mv.RIGHT
    if keys[pygame.K_UP]:
        mov = mv.UP
    if keys[pygame.K_DOWN]:
        mov = mv.DOWN






    pygame.display.update()
pygame.quit()
