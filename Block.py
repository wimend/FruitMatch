import pygame
from FruitConstants import FruitConstants

class Block:
    id = None               #An ID for the object
    name = None             #The name of the represented food
    foodNumber = None       #The ID  of the food
    representant = None     #The representation which may be displayed
    matched = False         #Indicates wether it is part of a match or not
    moviment = None         #Indicates what moviment the block is doing
    fc = FruitConstants()   #Used to load the fruit images

    def __init__(self, foodName=None, foodNumber=0, id=None):
        self.name = foodName
        self.setFoodNumber(foodNumber)
        self.id = id
        self.matched = False
        self.moviment = None

    def __eq__(self, other):
        """Defines that one Block object will be equal to other object if it has the same
        foodNumber value"""
        if isinstance(other, Block):
            return self.foodNumber == other.foodNumber and not self.matched and not other.matched

    def __str__(self):
        return str(self.foodNumber)

    def __int__(self):
        return self.foodNumber

    def setFoodNumber(self, foodNumber):
        self.foodNumber = foodNumber
        self.representant = self.fc.GET_FRUTA(self.foodNumber)
        self.matched = False


    def getFoodRepresentant(self):
        return self.representant

    def setMatched(self,matched):
        if isinstance(matched, bool):
            self.matched = matched
