import numpy as np
from Block import Block

class NumpyHelper:
    def getIndexesNdArray(self, width, hight):
        A = np.arange(width * hight)
        A.shape = (hight, width)
        return A

    def getBlocksAsNdArray(self, blocks):
        if isinstance(blocks, list) and isinstance(blocks[0], list) and isinstance(blocks[0][0], Block):
            numbersList = []
            for i in range(0,len(blocks)):
                for j in range(0,len(blocks[i])):
                    numbersList.append(int(blocks[i][j]))

            A = np.array(numbersList)
            A.shape = (len(blocks), len(blocks[0]))
