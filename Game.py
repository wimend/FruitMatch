from Board import Board
from Moviment import Moviment

class Game:
    board = None
    matchesX = []
    matchesY = []
    score = 0

    def __init__(self):
        self.board = Board(7,7)
        self.__checkMatches(self.board, True)
        self.score = 0

    def addMatchToX(self, positionY, initialX, finalX, fruitNumber):
        self.matchesX.append((positionY, initialX, finalX, fruitNumber, (finalX - initialX) * 100))

    def addMatchToY(self, positionX, initialY, finalY, fruitNumber):
        self.matchesY.append((positionX, initialY, finalY, fruitNumber, (finalY - initialY) * 100))

    def getMatchsX(self):
        return self.matchesX

    def getMatchsY(self):
        return self.matchesY

    def checkMatches(self):
        self.__checkMatches(self.board, False)

    def removeMatches(self):
        self.__removeMatches(self.board, True)

    def resetGame(self):
        self.board.resetBoard()
        self.__checkMatches(self.board, True)

    def setMatchedBlocksX(self, positionY, initialX, finalX):
        for x in range(initialX, finalX):
            self.board.getBlock(positionY,x).setMatched(True)

    def setMatchedBlocksY(self, positionX, initialY, finalY):
        for y in range(initialY, finalY):
            self.board.getBlock(y,positionX).setMatched(True)

    def unsetMatched(self):
        for y in range(self.board.getBoardHight()):
            for x in range(self.board.getBoardWidth()):
                self.board.getBlock(y,x).setMatched(False)

    def __checkMatches(self, board, auto=True):
        boardHight = board.getBoardHight()
        boardWidth = board.getBoardWidth()
        contadorSequenciaX = 0
        contadorSequenciaY = [0] * boardHight
        lastBlockX = None
        lastBlockY = [None] * boardHight
        for i in range(0, boardHight):
            if contadorSequenciaX > 2:
                self.addMatchToX(i - 1, boardWidth - contadorSequenciaX, boardWidth, lastBlockX.foodNumber)
            contadorSequenciaX = 0
            for j in range(0, boardWidth):

                if board.getBlock(i,j) == lastBlockX:
                    contadorSequenciaX += 1
                else:
                    if contadorSequenciaX > 2:
                        self.addMatchToX(i + 0, j - contadorSequenciaX, j, lastBlockX.foodNumber)
                    contadorSequenciaX = 1

                if lastBlockY[j] ==  board.getBlock(i,j):
                    contadorSequenciaY[j] += 1
                else:
                    if contadorSequenciaY[j] > 2:
                        self.addMatchToY(j, i - contadorSequenciaY[j], i, lastBlockY[j].foodNumber)
                    contadorSequenciaY[j]=1

                lastBlockX		=	board.getBlock(i,j)
                lastBlockY[j] 	=  	board.getBlock(i,j)

        if contadorSequenciaX > 2:
            self.addMatchToX(boardHight - 1, boardWidth - contadorSequenciaX, boardWidth, lastBlockX.foodNumber)
        for j in range(0, boardWidth):
            if contadorSequenciaY[j] > 2:
                self.addMatchToY(j, boardHight - contadorSequenciaY[j], boardHight, lastBlockY[j].foodNumber)
        for match in self.matchesX:
            self.setMatchedBlocksX(match[0],match[1],match[2])
        for match in self.matchesY:
            self.setMatchedBlocksY(match[0],match[1],match[2])
        if auto:
            while self.__removeMatches(board, False,False):
                self.__checkMatches(board)

    def __removeMatches(self, board, debug=False, scored=True):
        hasRemoved = False
        multiplier = len(self.matchesX) + len(self.matchesY)
        score = 0
        if len(self.matchesX) > 0:
            hasRemoved = True
            for match in self.matchesX:
                y = match[0]
                xInit = match[1]
                xFim = match[2]
                if scored:
                    score += (match[2] - match[1]) * 100
                if debug:
                    for x in range(xInit, xFim):
                        board.getBlock(y,x).foodNumber = "X"
                    print(board)
                for x in range(xInit, xFim):
                    board.gravity(y,x)
                self.matchesX[self.matchesX.index(match)] = (match[0] +1, match[1]+1, match[2], match[3])
            if debug:
                print("Match X: " + str(self.matchesX))
            self.matchesX = []

        if len(self.matchesY) > 0:
            hasRemoved = True
            for match in self.matchesY:
                x = match[0]
                yInit = match[1]
                yFim = match[2]
                if scored:
                    score += (match[2] - match[1]) * 100
                if debug:
                    for y in range(yInit, yFim):
                        board.getBlock(y,x).setFoodNumber("X")
                    print(board)
                for y in range(yInit, yFim):
                    board.gravity(y,x)
                self.matchesY[self.matchesY.index(match)] = (match[0] +1, match[1]+1, match[2], match[3])
            if debug:
                print("Match Y: " + str(self.matchesY))
            self.matchesY = []
        if scored:
            score = score * multiplier
            self.score += score
        self.unsetMatched()
        return hasRemoved


    def swapLeft(self, positionX, positionY):
        if not self.board.swap(positionX,positionY,Moviment.LEFT):
            print("This movement is not allowed")
        else:
            self.__checkMatches(self.board,False)

    def swapRight(self, positionX, positionY):
        if not self.board.swap(positionX,positionY,Moviment.RIGHT):
            print("This movement is not allowed")
        else:
            self.__checkMatches(self.board,False)

    def swapUp(self, positionX, positionY):
        if not self.board.swap(positionX,positionY,Moviment.UP):
            print("This movement is not allowed")
        else:
            self.__checkMatches(self.board,False)

    def swapDown(self, positionX, positionY):
        if not self.board.swap(positionX,positionY,Moviment.DOWN):
            print("This movement is not allowed")
        else:
            self.__checkMatches(self.board,False)

    def removeMatches(self):
        return self.__removeMatches(self.board)

    def printBoard(self):
        print(self.board)
