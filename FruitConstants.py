import pygame as pg

class FruitConstants:

    images = None
    quantity = None
    imageSize = None
    imagesFruits = None
    explosionsX = []
    explosionsY = []
    explosion = None
    def __init__(self):
        self.images = ["images/0.png", "images/1.png", "images/2.png", "images/3.png", "images/4.png", "images/5.png"]
        self.quantity = self.images.__len__()
        self.imageSize = self.__class__.GET_IMAGE_SIZE()
        self.imagesFruits = (pg.transform.scale(pg.image.load(self.images[0]),self.imageSize), pg.transform.scale(pg.image.load(self.images[1]),self.imageSize),
                             pg.transform.scale(pg.image.load(self.images[2]),self.imageSize), pg.transform.scale(pg.image.load(self.images[3]),self.imageSize),
                             pg.transform.scale(pg.image.load(self.images[4]),self.imageSize), pg.transform.scale(pg.image.load(self.images[5]),self.imageSize))
        self.explosion = pg.transform.scale(pg.image.load("images/exp.png"),self.imageSize)


    def GET_FRUTA(self, number):
        if not isinstance(number,int) or number < 0 or number >= self.quantity:
            return None
        return self.imagesFruits[number]

    def getDynamicExplosionX(self,number,fruit):
        s = pg.Surface((5 + self.imageSize[0] * number, self.imageSize[1]))
        b = pg.Surface((5 + self.imageSize[0] * number, self.imageSize[1]), pg.SRCALPHA)
        s.fill((255,255,255))
        b.fill(((242,239,206,170)))
        for i in range(number):
            #b.blit(self.GET_FRUTA(fruit),(self.imageSize[0] * i, 0))
            b.blit(self.explosion, (5 + self.imageSize[0] * i, 0))
        s.blit(b,(0,0))
        return s

    def getDynamicExplosionY(self,number,fruit):
        s = pg.Surface((self.imageSize[0], 5 + self.imageSize[1] * number))
        b = pg.Surface((self.imageSize[0], 5 + self.imageSize[1] * number), pg.SRCALPHA)
        s.fill((255,255,255))
        b.fill(((242,239,206,170)))
        for i in range(number):
            #b.blit(self.GET_FRUTA(fruit),(0, self.imageSize[0] * i))
            b.blit(self.explosion, (0, 5 + self.imageSize[0] * i))
        s.blit(b,(0,0))
        return s

    @staticmethod
    def GET_IMAGE_SIZE() -> tuple:
        return (70,70)
