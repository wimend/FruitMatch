import cx_Freeze

cx_Freeze.setup(
    name="Nefrutionhas",
    options={"build_exe":{"packages":["pygame","random"],
                          "include_files":["images/0.png", "images/1.png", "images/2.png", "images/3.png",
                                           "images/4.png", "images/5.png", "images/exp.png",
                                            "audios/match.wav", "audios/click.wav", "audios/tick.wav",
                                           "audios/tock.wav", "audios/Brave World.wav"]}},
    executables=[cx_Freeze.Executable("StartGame.py")],
    version="1.0.0"
)
