from Moviment import Moviment
from Block import Block
from random import randint
from FruitConstants import FruitConstants

class Board:
    BOARD_WIDTH = 5
    BOARD_HIGHT = 5
    blocks = []
    fc = None
    fruits = 0

    def __init__(self,width=5, high=5):
        self.BOARD_WIDTH = width
        self.BOARD_HIGHT = high
        self.fc = FruitConstants()
        for i in range(0,int(high)):
            for j in range(0,int(width)):
                self.fruits = min(len(self.fc.imagesFruits) - 1,width)
                num = self.myRandom(0, self.fruits)
                if j == 0:
                    self.blocks.append([Block(None,num,i*width + j)])
                else:
                    self.blocks[i].insert(j,Block(None,num,i*width + j))

    def __str__(self):
        text = ""
        for i in range(0,int(self.BOARD_HIGHT)):
            text += " | "
            for j in range(0,int(self.BOARD_WIDTH)):
                text += str(self.blocks[i][j]) + " | "
            text += " \n"
        return text

    def getBoardWidth(self):
        return self.BOARD_WIDTH

    def getBoardHight(self):
        return self.BOARD_HIGHT

    def getBlocks(self):
        return self.blocks

    def getBlock(self, x, y) -> Block:
        return self.blocks[x][y]

    def resetBoard(self):
        for i in range(0,int(self.BOARD_HIGHT)):
            for j in range(0,int(self.BOARD_WIDTH)):
                self.blocks[i][j].setFoodNumber(self.myRandom(0, self.fruits))



    def swap(self, positionX, positionY, mov):
        if not isinstance(mov,Moviment):
            return False

        xComplement, yComplement = mov._value_

        #############################################Start Checking#####################################################
        # Check if the moviment is possible according to board limits
        checkSwap = positionX + xComplement >= self.BOARD_HIGHT or positionY + yComplement >= self.BOARD_WIDTH
        checkSwap = checkSwap or positionX + xComplement < 0 or positionY + yComplement < 0

        if checkSwap:
            return False
        ##############################################End Checking######################################################

        bufferBlock = self.blocks[positionX + xComplement][positionY + yComplement]
        self.blocks[positionX + xComplement][positionY + yComplement] = self.blocks[positionX][positionY]
        self.blocks[positionX][positionY] = bufferBlock
        return True


    def __gravity(self,y,x):
        ret = True
        yNew = y
        while(ret):
            ret = self.swap(yNew,x,Moviment.UP)
            yNew -= 1
        yNew = 0
        self.blocks[yNew][x].setFoodNumber(self.myRandom(0, self.fruits))



    def myRandom(self, init, fin):
        if init > fin:
            aux = init
            init = fin
            fin = aux

        a = randint(init, fin)
        b = randint(init, fin)
        if a > b:
            return randint(b,a)
        return randint(a,b)

    def gravity(self, y, x):
        self.__gravity(y,x)
