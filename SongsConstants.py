import pygame as pg

class SongsConstants:
    songs = None
    songVolume = None

    def __init__(self):
        self.songs = {"match" : pg.mixer.Sound("audios/match.wav"), "click": pg.mixer.Sound("audios/click.wav"), "tick" : pg.mixer.Sound("audios/tick.wav"), "tock" : pg.mixer.Sound("audios/tock.wav")}

    def setSongsVolume(self, volume):
        self.songVolume = volume


    def playSound(self,name):
        if self.songs.get(name) is not None:
            if self.songVolume is not None:
                self.songs.get(name).set_volume(self.songVolume)
            self.songs.get(name).play()
