import pygame as pg
import os
from pygame.sysfont import SysFont
from Game import Game
from FruitConstants import FruitConstants
from SongsConstants import SongsConstants
from BoardGraphics import BoardGraphics

class GameGraphics:
    game = None
    board = None
    window = None
    outSide = False
    backGround = None
    mouseDownPos = None
    maxFruitPositionHorizontal = None
    maxFruitPositionVertical = None
    firstClick = None
    DISPLAY_WIDTH = 650
    DISPLAY_HIGHT = 710
    DISPLAY_DIMENTIONS = (DISPLAY_WIDTH,DISPLAY_HIGHT)
    FRUIT_MARGIN = (40,100)
    GAME_NAME = "FrutaSe"
    fc = FruitConstants()
    somador = 40
    contador = 0
    totalTime = 15000
    updateTimeFrame = 1000
    updateTime = None
    tempo = totalTime
    sys_font  = None
    sc = None
    firstClicked = None
    mouseDownGridPos = None
    possibleMove = None
    mouseUp = False
    maxTimeSize = None
    lastSecond = None
    musicVolume = None
    reseted = False
    playerName = None
    highScore = None


    def __init__(self, outSide=False, window=None):
        os.environ['SDL_VIDEO_CENTERED'] = '1'
        self.game = Game()
        self.DISPLAY_WIDTH = self.game.board.getBoardWidth() * FruitConstants.GET_IMAGE_SIZE()[0] + self.FRUIT_MARGIN[0] * 2
        self.DISPLAY_HIGHT = self.game.board.getBoardHight() * FruitConstants.GET_IMAGE_SIZE()[1] + self.FRUIT_MARGIN[1] + self.FRUIT_MARGIN[0]
        self.DISPLAY_DIMENTIONS = (self.DISPLAY_WIDTH,self.DISPLAY_HIGHT)
        pg.init()
        self.window = pg.display.set_mode(self.DISPLAY_DIMENTIONS)

        pg.display.set_caption(self.GAME_NAME)

        self.board = BoardGraphics(self.window, self.game.board, self.FRUIT_MARGIN)
        self.maxFruitPositionHorizontal = self.FRUIT_MARGIN[0] + (self.game.board.getBlocks()[0].__len__()) * FruitConstants.GET_IMAGE_SIZE()[0]
        self.maxFruitPositionVertical = self.FRUIT_MARGIN[1] + (self.game.board.getBlocks().__len__()) * FruitConstants.GET_IMAGE_SIZE()[1]
        self.sys_font = SysFont(None,60)
        tmpf = SysFont(None,135)
        t = tmpf.render("0",0,(0,0,0))
        self.maxTimeSize = (t.get_width(),t.get_height())
        self.sc = SongsConstants()
        self.updateTime = self.totalTime // self.updateTimeFrame
        self.lastSecond = self.totalTime // 1000
        self.musicVolume = 1
        pg.display.update()
        self.outSide = outSide

    def setTotalTime(self, time):
        self.totalTime = time * 1000
        self.updateTime = self.totalTime // self.updateTimeFrame
        self.lastSecond = time
        self.tempo = self.totalTime

    def resetGame(self):
        self.game.resetGame()
        self.updateTime = self.totalTime // self.updateTimeFrame
        self.lastSecond = self.totalTime // 1000
        self.tempo = self.totalTime
        self.game.score = 0
        self.reseted = True
        self.playerName = None
        self.highScore = None


    def setMusicVolume(self, volume):
        pg.mixer.music.set_volume(volume)
        self.musicVolume = volume

    def setSoundsVolume(self, volume):
        self.sc.setSongsVolume(volume)

    def start(self):
        run = True
        if self.reseted:
            self.window = pg.display.set_mode(self.DISPLAY_DIMENTIONS)
            self.reseted = False
        self.printMatchedBoard()
        pg.mixer.music.load("audios/Brave World.wav")
        pg.mixer.music.play(-1)
        try:
            while run:
                self.playTick()
                if self.tempo > 0 or len(self.game.getMatchsX()) > 0 or len(self.game.getMatchsY()) > 0:
                    run = self.handleEvent(pg.event.get())
                    shouldPrint = len(self.game.getMatchsX()) >  0 or len(self.game.getMatchsY()) > 0
                    shouldPrint = shouldPrint or self.mouseDownPos is not None or self.firstClicked is not None or self.mouseUp or self.possibleMove is not None
                    pg.time.delay(40)
                    if shouldPrint:
                        self.printMatchedBoard()
                        self.mouseUp = False
                        self.firstClicked = None
                    else:
                        self.printTime()
                    if self.tempo > 0:
                        self.tempo -= 40
                elif self.contador < 150:
                    if self.mouseDownPos is not None or self.firstClicked is not None or self.mouseUp or self.possibleMove is not None:
                        self.resetValue()
                        self.printMatchedBoard()
                    if self.contador == 0:
                        self.highScore = self.writeScore()
                        if self.highScore is not None:
                            self.playerName = ""
                            self.printPlayerName()
                            self.contador = self.contador + 1
                        else:
                            self.printEndScore()
                    if self.highScore is not None:
                        ret = self.getPlayerName(pg.event.get())
                        if ret == -1:
                            run = False
                        elif ret == 1:
                            self.printPlayerName()
                    else:
                        run = self.handleEndEvent(pg.event.get())
                        self.contador = self.contador + 1
                    pg.time.delay(40)
                else:
                    run = False

            if self.highScore is not None:
                self.writePlayername()
                self.highScore = None
                self.playerName = None
            self.game.unsetMatched()
            self.resetValue()
            pg.mixer.music.stop()
            if not self.outSide:
                pg.quit()
        except Exception as e:
            print(e)
            print("O jogo Crashou!!")
            pg.quit()


    def getPlayerName(self, events):
        for event in events:
            if event.type == pg.QUIT:
                return -1
            if event.type == pg.KEYDOWN:
                if event.key == pg.K_ESCAPE or event.key == pg.K_RETURN:
                    return -1
                if  event.key == pg.K_BACKSPACE:
                    self.playerName = self.playerName[:-1]
                    return 1
                else:
                    if len(self.playerName) < 20:
                        self.playerName += event.unicode
                        return 1
        return 0

    def writePlayername(self):
        lines = None
        try:
            f = open("score_names.txt","r")
            lines = f.readlines()
            f.close()
        except IOError as x:
            lines = []
            f = open("score_names.txt","w")
            f.close()
            print("Nao foi possivel atualizar arquivo de pontuacao")
        lines.insert(self.highScore,self.playerName + "\n")
        lines = lines[0:10]
        w = open("score_names.txt","w")
        w.writelines(lines)
        w.close()

    def writeScore(self):
        scoreFileRead = None
        playerPlace = None
        try:
            scoreFileRead = open("score.txt","r")
        except IOError as x:
            f = open("score.txt","w")
            f.close()
            scoreFileRead = open("score.txt","r")
        lines = scoreFileRead.readlines()
        scoreFileRead.close()
        lines.append(str(self.game.score) + "\n")
        intLines = sorted(list(map(int, lines)))
        lines = list(map(str, reversed(intLines)))
        lines = lines[0:10]
        maxPlace = min(9,len(lines) - 1)
        intLines = sorted(list(map(int, lines)))
        if intLines.count(self.game.score) > 0:
            playerPlace = maxPlace - intLines.index(self.game.score)
        else:
            playerPlace = None
        writeFile = open("score.txt","w")
        for line in lines:
            writeFile.write(line + "\n")
        writeFile.close()
        return playerPlace

    def resetValue(self):
        self.mouseDownPos = self.firstClicked =  self.possibleMove = None
        self.mouseUp = False

    def printPlayerName(self):
        fonts = pg.font.SysFont(None, self.game.board.getBoardWidth() * 7)
        timeIsUp = "Novo Recorde! " + str(self.game.score) + " Pontos!"
        yourScore = "Informe Seu Nome:"
        yourName = self.playerName
        texto = fonts.render(timeIsUp,0,(0,0,255))
        texto2 = fonts.render(yourScore,0,(0,0,255))
        texto3 = fonts.render(yourName,0,(255,0,0))
        txtWidth = max(texto.get_width(), texto2.get_width(), texto3.get_width())
        s = pg.Surface((txtWidth, texto.get_height() + texto2.get_height() + texto3.get_height()), pg.SRCALPHA)
        s.fill((242,239,206))
        s.blit(texto,(txtWidth//2  - texto.get_width() // 2,0))
        #s.blit(texto,(0,0))
        s.blit(texto2, (txtWidth//2  - texto2.get_width() // 2,texto.get_height()))
        s.blit(texto3, (txtWidth//2  - texto3.get_width() // 2,texto.get_height() + texto2.get_height()))
        self.window.blit(s, (self.DISPLAY_WIDTH//2 - s.get_width()//2, self.DISPLAY_HIGHT//2 - s.get_height()//2))
        #self.window.blit(texto, (self.DISPLAY_WIDTH//2 - texto.get_width()//2, self.DISPLAY_HIGHT//2 + texto.get_height()//2))
        pg.display.update()

    def printEndScore(self):
        fonts = pg.font.SysFont(None, self.game.board.getBoardWidth() * 8)
        timeIsUp = "O tempo acabou!"
        yourScore = "Sua pontuação foi: " + str(self.game.score)
        texto = fonts.render(timeIsUp,0,(0,0,255))
        texto2 = fonts.render(yourScore,0,(0,0,255))
        txtWidth = max(texto.get_width(), texto2.get_width())
        s = pg.Surface((txtWidth, texto.get_height() + texto2.get_height()), pg.SRCALPHA)
        s.fill((242,239,206,170))
        s.blit(texto,(txtWidth//2  - texto.get_width() // 2,0))
        #s.blit(texto,(0,0))
        s.blit(texto2, (txtWidth//2  - texto2.get_width() // 2,texto.get_height()))
        self.window.blit(s, (self.DISPLAY_WIDTH//2 - s.get_width()//2, self.DISPLAY_HIGHT//2 - s.get_height()//2))
        #self.window.blit(texto, (self.DISPLAY_WIDTH//2 - texto.get_width()//2, self.DISPLAY_HIGHT//2 + texto.get_height()//2))
        pg.display.update()


    def handleEvent(self, events):
        for event in events:
            if event.type == pg.QUIT or (event.type == pg.KEYDOWN and event.key == pg.K_ESCAPE):
                return False
            if event.type == pg.MOUSEBUTTONDOWN:
                self.mouseDownAct()
            if event.type == pg.MOUSEBUTTONUP:
                self.mouseUpAct()
            if event.type == pg.MOUSEMOTION:
                if self.mouseDownPos is not None:
                    pos = pg.mouse.get_pos()
                    mpos = self.board.getGridPosition(pos[0],pos[1])
                    if mpos != self.mouseDownGridPos and self.board.checkNeighbor(self.mouseDownGridPos,mpos):
                        self.possibleMove = self.board.getScreenPosition(mpos[1],mpos[0])
                    else:
                        self.possibleMove = None

        return True


    def handleEndEvent(self, events):
        for event in events:
            if event.type == pg.QUIT or (event.type == pg.KEYDOWN and event.key == pg.K_ESCAPE):
                return False
        return True

    def mouseDownAct(self):
        dx,dy = pg.mouse.get_pos()
        if dx < self.FRUIT_MARGIN[0] or dx > self.maxFruitPositionHorizontal or dy < self.FRUIT_MARGIN[1] or dy > self.maxFruitPositionVertical:
            self.mouseDownPos = None
            self.mouseDownGridPos = None
            self.firstClick = None
            self.firstClicked = None
            return
        self.mouseDownGridPos = self.board.getGridPosition(dx,dy)
        if self.board.getBlock(self.mouseDownGridPos[1],self.mouseDownGridPos[0]).matched:
            self.mouseDownPos = None
            self.mouseDownGridPos = None
            self.firstClick = None
            return
        self.mouseDownGridPos = self.board.getGridPosition(dx,dy)
        self.mouseDownPos = self.board.getScreenPosition(self.mouseDownGridPos[1],self.mouseDownGridPos[0])




    def mouseUpAct(self):
        if self.mouseDownGridPos is None:
            return
        posDown = self.mouseDownGridPos
        self.possibleMove = None
        self.mouseDownPos = None
        self.mouseDownGridPos = None
        self.mouseUp = True

        pos = pg.mouse.get_pos()
        ux,uy = pos
        posUp = self.board.getGridPosition(ux,uy)
        same = posDown == posUp
        if same:
            self.click(posUp)
        else:
            self.sc.playSound("click")
            self.moveNeighbor(posUp,posDown)
            self.firstClick = None

    def click(self, pos):
        self.sc.playSound("click")
        if self.firstClick is None:
            self.firstClick = pos
            self.firstClicked = True
        else:
            self.moveNeighbor(pos)
            self.firstClick = None

    def moveNeighbor(self, pos, firstClick=None):
        if firstClick is None:
            firstClick = self.firstClick
        if not self.board.checkNeighbor(pos,firstClick) or self.board.getBlock(pos[1],pos[0]).matched:
            return False
        fy,fx = firstClick
        sy,sx = pos
        dx = sx - fx
        dy = sy - fy
        if dx == -1 and dy == 0:
            self.game.swapUp(fx,fy)
        elif dx == 1 and dy == 0:
            self.game.swapDown(fx,fy)
        elif dx == 0 and dy == -1:
            self.game.swapLeft(fx,fy)
        elif dx == 0 and dy == 1:
            self.game.swapRight(fx,fy)
        return False

    def printMatchedBoard(self):
        self.printBoard()
        if self.contador < 240 and (len(self.game.getMatchsX()) > 0 or len(self.game.getMatchsY()) > 0):
            if self.contador < self.somador:
                self.sc.playSound("match")
            multiplier = len(self.game.getMatchsX()) + len(self.game.getMatchsY())
            for match in self.game.getMatchsX():
                s = self.fc.getDynamicExplosionX(match[2] - match[1], match[3])
                s.set_alpha(self.contador)
                x,y = self.board.getScreenPosition(match[0],match[1])
                self.window.blit(s, (x,y))
                self.window.blit(self.getDynamicScore(match[4], self.contador/4, multiplier), (x - self.contador/4, y))
            for match in self.game.getMatchsY():
                s = self.fc.getDynamicExplosionY(match[2] - match[1], match[3])
                s.set_alpha(self.contador)
                x,y = self.board.getScreenPosition(match[1],match[0])
                self.window.blit(s, (x,y))
                self.window.blit(self.getDynamicScore(match[4], self.contador/4, multiplier), (x- self.contador/4, y))
            self.contador += self.somador
        else:
            if (len(self.game.getMatchsX()) > 0 or len(self.game.getMatchsY()) > 0):
                self.game.removeMatches()
                self.game.checkMatches()
                self.printBoard()
            self.contador = 0
        self.printTempo(self.tempo, self.tempo < 10000)

    def printBoard(self):
        if self.backGround is not None:
            self.window.blit(self.backGround,(0,0))
        else:
            self.window.fill((242,239,206))
        self.printScore()
        self.board.boardPrint(self.possibleMove,self.mouseDownPos,self.firstClick)
        #self.printTime()

    def printScore(self):
        text = self.sys_font.render(str(self.game.score),0,(0,255,0))
        s = pg.Surface((120,40), pg.SRCALPHA)
        s.fill((0,0,0,180))
        s.blit(text,(0,0))
        self.window.blit(s,(30,30))

    def getDynamicScore(self, score, size=0, multiplier=1):
        color = None
        myFont = pg.font.SysFont(None, 50 + int(size))
        score = score * multiplier
        if score >= 0:
            color = (0,255,0)
            points = "+" + str(score)
        else:
            color = (255,0,0)

        text = myFont.render(points,0,color)
        s = pg.Surface((text.get_width(), text.get_height()), pg.SRCALPHA)
        s.fill((255,255,255,0))
        s.blit(text,(0,0))
        return s

    def printTime(self):
        if self.tempo < 10000 and self.updateTimeFrame != 200:
            self.updateTimeFrame = 200
            self.updateTime = self.tempo // self.updateTimeFrame
            self.printTempo(self.tempo, self.tempo < 10000)
        elif self.updateTime > self.tempo // self.updateTimeFrame:
            self.printTempo(self.tempo, self.tempo < 10000)
            self.updateTime = self.tempo // self.updateTimeFrame

    def playTick(self):

        if self.tempo > 0 and self.tempo < 10000 and self.tempo // 1000 < self.lastSecond:
            self.lastSecond = self.tempo // 1000
            if self.musicVolume > 0.0 and self.musicVolume - 0.2 > 0.0:
                self.musicVolume = self.musicVolume - 0.2
            else:
                self.musicVolume = 0.0
            pg.mixer.music.set_volume(self.musicVolume)
            self.sc.playSound("tock")


    def printTempo(self, time, sized):
        color = (0,0,255)
        seconds = time//1000
        myFont = None
        b = None
        if sized:
            color = (255,0,0)
            size = 5 - (time % 1000) // 200
            size = size * 15
            funt = pg.font.SysFont(None, 60 + int(size))
            text2 = funt.render(str(seconds),0,color)
            b = pg.Surface(self.maxTimeSize, pg.SRCALPHA)
            b.fill((242,239,206,255))
            self.window.blit(b,(self.DISPLAY_WIDTH // 2, 0))
            b.fill((255,255,255,0))
            b.blit(text2, (0,0))
            self.window.blit(b,(self.DISPLAY_WIDTH // 2, 0))
        else:
            myFont = pg.font.SysFont(None, 60)
            text = myFont.render(str(seconds),0,color)
            s = pg.Surface((50,45), pg.SRCALPHA)
            s.fill((242,239,206,255))
            self.window.blit(s,(self.DISPLAY_WIDTH // 2, 0))
            s.fill((255,255,255,0))
            s.blit(text,(0,0))
            self.window.blit(s,(self.DISPLAY_WIDTH // 2, 0))
        pg.display.update()


if __name__ == '__main__':
    #os.environ['SDL_VIDEO_CENTERED'] = '1'
    gg = GameGraphics(True)
    gg.setMusicVolume(0)
    gg.setSoundsVolume(0)
    gg.start()
    pg.quit()
