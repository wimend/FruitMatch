import pygame as pg
import os

class FileReader:
    lista = None
    nomes = None
    def __init__(self, fileName1, fileName2=None):
        try:
            self.nomes = [open(fileName1,"r")]
            if fileName2 is not None:
                self.nomes += [open(fileName2,"r")]
        except IOError as x:
            self.lista = []

    def getInfo(self, dimention=None):
        self.lista = []
        if self.nomes is not None:
            auxLista = self.nomes[0].readlines()
            nomeLista = None
            if len(self.nomes) > 1:
                nomeLista = self.nomes[1].readlines()
            for ind in range(len(auxLista)):
                if nomeLista is not None:
                    linha = str(ind + 1) + " - " + nomeLista[ind] + ": " + auxLista[ind]+ "\n"
                    self.lista.append(linha)
                else:
                    self.lista.append(auxLista[ind])
        return self.getSurfaceListed(self.lista, dimention)

    def getSurfaceListed(self, informedList, dimention=None):
        window = None
        bg = pg.image.load("images/botao.png")
        if dimention is not None:
            window = pg.Surface(dimention)
            bg = pg.transform.scale(bg,dimention)
        else:
            window = pg.Surface((600,600))
            bg = pg.transform.scale(bg,(600,600))

        window.blit(bg,(0,0))
        font = pg.font.Font(None, 35)
        text = None
        contador = 0
        for nome in informedList:
            text = font.render(str(nome).replace("\n",""), 0, (2,104,36))
            window.blit(text,(300 - text.get_width()//2, 60 + text.get_height() * contador))
            contador = contador + 1
        return window


    def getSurfaceOption(self, informedList, dimention=None):
        window = None
        bg = pg.image.load("images/botao.png")

        if dimention is not None:
            window = pg.Surface(dimention)
            bg = pg.transform.scale(bg,dimention)
        else:
            window = pg.Surface((600,600))
            bg = pg.transform.scale(bg,(600,600))

        window.blit(bg,(0,0))
        font = pg.font.Font(None, 45)
        text = None
        contador = 0
        for nome in informedList:
            text = font.render(str(nome).replace("\n",""), 0, (2,104,36))
            window.blit(text,(300 - text.get_width()//2, 60 + text.get_height() * contador))
            contador = contador + 1
        return window




if __name__ == '__main__':
    os.environ['SDL_VIDEO_CENTERED'] = '1'
    pg.init()
    screen = pg.display.set_mode((700,700))
    ct = FileReader("nomes.txt")
    screen.blit(ct.getInfo(), (50,50))
    pg.display.update()
    pg.time.delay(5000)
    pg.quit()
