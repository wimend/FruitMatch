import pygame as pg
from FruitConstants import FruitConstants

class BoardGraphics:
    board = None
    window = None
    backGroundColor = None
    FRUIT_MARGIN = None
    contador = 0
    somador = 1
    totalTime = 30000
    timeBorder = None


    def __init__(self, windowSurface, board, margin=(0,0)):
        if windowSurface is None or board is None:
            pg.quit()
            return
        self.window = windowSurface
        self.board = board
        self.backGroundColor = pg.Surface((self.board.getBlocks()[0].__len__() * FruitConstants.GET_IMAGE_SIZE()[0] + 40, self.board.getBlocks().__len__() * FruitConstants.GET_IMAGE_SIZE()[1] + 40), pg.SRCALPHA)
        self.timeBorder = (self.board.getBlocks()[0].__len__() * FruitConstants.GET_IMAGE_SIZE()[0] + 45, self.board.getBlocks().__len__() * FruitConstants.GET_IMAGE_SIZE()[1] + 45)
        self.backGroundColor.fill((0,0,180,170))
        self.FRUIT_MARGIN = margin

    def boardPrint(self, possibleMove=None, mouseDownPos=None,firstClick=None):
        #self.window.blit(self.backGroundColor,(self.FRUIT_MARGIN[0] -20, self.FRUIT_MARGIN[1]-20))
        for i in range(0,self.board.getBoardHight()):
            for j in range(0,self.board.getBoardWidth()):
                self.window.blit(self.getFruit(i,j),self.getScreenPosition(i,j))

        if possibleMove is not None:
            pg.draw.rect(self.window,(0,255,0),pg.Rect(possibleMove,FruitConstants.GET_IMAGE_SIZE()),5)
        if mouseDownPos is not None:
            pg.draw.rect(self.window,(0,0,255),pg.Rect(mouseDownPos,FruitConstants.GET_IMAGE_SIZE()),5)
        elif firstClick is not None:
            pg.draw.rect(self.window,(0,0,255),pg.Rect(self.getScreenPosition(firstClick[1],firstClick[0]),FruitConstants.GET_IMAGE_SIZE()),5)
        #pg.display.update()

    def getFruit(self,i,j):
        return self.board.getBlock(i,j).getFoodRepresentant()

    def getScreenPosition(self,i,j):
        fx = self.FRUIT_MARGIN[0] + j * FruitConstants.GET_IMAGE_SIZE()[0]
        fy = self.FRUIT_MARGIN[1] + i * FruitConstants.GET_IMAGE_SIZE()[1]
        return (fx,fy)

    def getGridPosition(self,x,y):
        fx = (x - self.FRUIT_MARGIN[0])//FruitConstants.GET_IMAGE_SIZE()[0]
        fy = (y - self.FRUIT_MARGIN[1])//FruitConstants.GET_IMAGE_SIZE()[1]
        return (fx,fy)

    def checkNeighbor(self,t1,t2):
        fy,fx = t1
        sy,sx = t2
        if fy < 0 or sy < 0 or fy >= self.board.getBoardWidth() or sy >= self.board.getBoardWidth():
            return False
        if fx < 0 or sx < 0 or fx >= self.board.getBoardHight() or sx >= self.board.getBoardHight():
            return False
        dx = sx - fx
        dy = sy - fy
        if dx == -1 and dy == 0:
            return True
        elif dx == 1 and dy == 0:
            return True
        elif dx == 0 and dy == -1:
            return True
        elif dx == 0 and dy == 1:
            return True
        return False

    def getBlock(self,i,j):
        return self.board.getBlock(i,j)








